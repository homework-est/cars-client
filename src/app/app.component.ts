import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    userActive: boolean = false;
    carActive: boolean = false;

    constructor() {

    }

    hightlightLink(link: string): void {

        this.userActive = (link === 'user') ? true : false;
        this.carActive = (link === 'car') ? true : false;
    }

}
