import { User } from './user.model';

export class UserData {

    users: Array<User>;
    pageSize: number;
    page: number;
    total: number;
}
