export class FilterCarRequest {

    query: string;
    filter = 'model';
    sortDirection = 'DESC';
}
