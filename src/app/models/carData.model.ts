import { Car } from './car.model';

export class CarData {

    cars: Array<Car>;
    pageSize: number;
    page: number;
    total: number;
}
