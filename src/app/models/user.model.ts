import { Car } from './car.model';

export class User {

    id: string;
    name: string;
    cars: Array<Car>;
}
