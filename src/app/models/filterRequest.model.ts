export class FilterRequest {

    query: string;
    filter = 'name';
    sortDirection = 'DESC';
}
