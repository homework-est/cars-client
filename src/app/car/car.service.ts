import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CarData } from '../models/carData.model';


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CarService {

    constructor(private http: HttpClient) { }

    private userUrl = '/api/v1/car';

    public getCars() {

        return this.http.get<CarData>(this.userUrl + '/get');
    }

    public getPagedCars(pageNo) {

        return this.http.get<CarData>(this.userUrl + '/get/page/' + pageNo);
    }

    public filterCars(filterRequest, pageNo) {

        return this.http.post<CarData>(this.userUrl + '/get/page/' + pageNo, filterRequest);
    }

}
