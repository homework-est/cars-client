import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CarData } from '../models/carData.model';
import { Car } from '../models/car.model';
import { FilterCarRequest } from '../models/filterCarRequest.model';
import { CarService } from './car.service';

@Component({
    selector: 'app-car',
    templateUrl: './car.component.html',
    styles: []
})
export class CarComponent implements OnInit {

    carData: CarData = new CarData();
    car: Car;
    cars: Car[];
    pageNo: number = 1;
    pageArray: number[];
    filterCarRequest: FilterCarRequest = new FilterCarRequest();

    constructor(private router: Router, private carService: CarService) {

    }

    ngOnInit() {
        this.carService.getCars()
            .subscribe(data => {
                console.log(data);
                if (data == null) {
                    this.handleNullCarData()
                } else {
                    this.carData = data;
                    this.constructPageArray();
                }
            },
                err => {
                    alert('there is some error');
                });
    }

    filterCars() {
        this.carService.filterCars(this.filterCarRequest, this.pageNo)
            .subscribe(data => {
                if (data == null) {
                    this.handleNullCarData();
                } else {
                    this.carData = data;
                    this.constructPageArray();
                }
            },
                err => {
                    alert('there is some error');
                });
    }

    getPagedCars() {
        this.carService.getPagedCars(this.pageNo)
            .subscribe(data => {
                if (data == null) {
                    this.handleNullCarData();
                } else {
                    this.carData = data;
                    this.constructPageArray();
                }
            },
                err => {
                    alert('there is some error');
                });
    }

    setPageNo(pageNo: number) {
        this.pageNo = pageNo;

        if (this.filterCarRequest.query === '') {
            this.getPagedCars();
        } else {
            this.filterCars();
        }
    }
    constructPageArray() {
        this.pageArray = new Array();
        this.pageArray = Array.from(Array(this.carData.total), (x, i) => (i + 1));
    }

    handleNullCarData() {
        this.carData = new CarData();
        alert('No data found or some problems have occured.');
    }
}


