import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { AppRoutingModule } from './app.routing.module';
import { UserService } from './user/user.service';
import { CarService } from './car/car.service';
import { HttpClientModule } from '@angular/common/http';
import { CarComponent } from './car/car.component';

@NgModule({
    declarations: [
        AppComponent,
        UserComponent,
        CarComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [UserService, CarService],
    bootstrap: [AppComponent]
})
export class AppModule { }
