import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserData } from '../models/userData.model';
import { User } from '../models/user.model';
import { Car } from '../models/car.model';
import { FilterRequest } from '../models/filterRequest.model';
import { UserService } from './user.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styles: []
})
export class UserComponent implements OnInit {

    carsShowed: boolean = false;
    userData: UserData = new UserData();
    user: User;
    cars: Car[];
    pageNo: number = 1;
    pageArray: number[];
    filterRequest: FilterRequest = new FilterRequest();

    constructor(private router: Router, private userService: UserService) {

    }

    ngOnInit() {
        this.userService.getUsers()
            .subscribe(data => {
                if (data == null) {
                    this.handleNullUserData();
                } else {
                    this.userData = data;
                    this.constructPageArray();
                }
            },
                err => {
                    alert('there is some error');
                });
    }

    showCars(user: User): void {
        this.carsShowed = true;
        this.cars = user.cars;
        this.user = user;
    }

    filterUsers() {
        this.carsShowed = false;
        this.userService.filterUsers(this.filterRequest, this.pageNo)
            .subscribe(data => {
                if (data == null) {
                    this.handleNullUserData();
                } else {
                    this.userData = data;
                    this.constructPageArray();
                }
            },
                err => {
                    alert('there is some error');
                });
    }

    getPagedUsers() {
        this.carsShowed = false;
        this.userService.getPagedUsers(this.pageNo)
            .subscribe(data => {
                if (data == null) {
                    this.handleNullUserData();
                } else {
                    this.userData = data;
                    this.constructPageArray();
                }
            },
                err => {
                    alert('there is some error');
                });
    }

    setPageNo(page: number) {

        this.pageNo = page;
        if (this.filterRequest.query === '') {
            this.getPagedUsers();
        } else {
            this.filterUsers();
        }
    }
    constructPageArray() {
        this.pageArray = new Array();
        this.pageArray = Array.from(Array(this.userData.total), (x, i) => (i + 1));
    }

    handleNullUserData() {
        this.userData = new UserData();
        alert('No data found or some problems have occured.');
    }
}


