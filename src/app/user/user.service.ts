import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { UserData } from '../models/userData.model';


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    private userUrl = '/api/v1/user';

    public getUsers() {
        return this.http.get<UserData>(this.userUrl + '/get');
    }

    public getPagedUsers(pageNo) {
        return this.http.get<UserData>(this.userUrl + '/get/page/' + pageNo);
    }

    public filterUsers(filterRequest, pageNo) {
        return this.http.post<UserData>(this.userUrl + '/get/page/' + pageNo, filterRequest);
    }

}
