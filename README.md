# Car Information Homework front end on Angular

Front end consists of two screens which displays users and their cars data. Data can be filtered and sorted using server side methods.

## Installation

For Development server Installation:

```bash
npm install
npm start
```

## Usage

Before you hit below url on browser, please make sure server is running.

```browser
Please open http://localhost:4200 on browser
```

## API End Points
```API
Users:
GET http://localhost:8080/api/v1/user/get - get first page
GET http://localhost:8080/api/v1/user/get/page/{pageNo} - get x page
GET http://localhost:8080/api/v1/user/get/{userId} - get user by Id
POST http://localhost:8080/api/v1/user/get/page/{pageNo} - get filtered and sorted users
request json : {"query":"z", "filter":"name", "sortDirection":"DESC"}

Cars:
GET http://localhost:8080/api/v1/car/get - get first page
GET http://localhost:8080/api/v1/car/get/page/{pageNo} - get x page
GET http://localhost:8080/api/v1/car/get/{userId} - get car by Id
POST http://localhost:8080/api/v1/car/get/page/{pageNo} - get filtered and sorted car
request json : {"query":"z", "filter":"make/model", "sortDirection":"DESC"}
```

## Filter and sorted method
I chose to have server side filtering and sorting along with pagination. I usually prefer server side operations to ensure consistent performance for large databases. Server side operations also give most recent and accurate data.

## Server-side (Back-end)

Server side code is written separately. It can be accessed using below gitlab repository.

```git client
https://gitlab.com/homework-est/carsinfo.git
``` 

Please fire below command on above repository to generate compiled code. Once done then all files under 'dist' folder should be copied to resources/static folder of this project. 

```bash
ng build --prod
``` 

## Future Tasks
Client side unit tests are not written.

## License
[MIT](https://choosealicense.com/licenses/mit/)
